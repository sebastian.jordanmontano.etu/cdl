from ast import arg
import json
import os
import sys
import argparse


FILE_NAME_CONSTANT = './tp1/default_configuration.json'
# Asigner par défaut comme fichier de configuration la constante
configuration_file_path = FILE_NAME_CONSTANT

parser = argparse.ArgumentParser()
parser.add_argument('--configuration', type=str)
configuration_argument = parser.parse_args().configuration

# Remplacer le chemin du fichier de configuration par les arguments ou une variable environnementale, s'ils existent.
if (configuration_argument):
    configuration_file_path = configuration_argument
elif (os.environ.get('FICHIER_DE_CONFIGURATION')):
    configuration_file_path = os.environ.get('FICHIER_DE_CONFIGURATION')

with open(configuration_file_path) as configuration_file:
    configuration = json.load(configuration_file)
    print(configuration)
