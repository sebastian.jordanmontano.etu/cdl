# Configuration de Logiciels - TP3

## Docker composer

Le fichier de configuration pour deployer une instance de WordPress se trouve en [docker-context/docker-compose.yml](./docker-context/docker-compose.yml). Là-bas la configuration de comment exécuter docker est definie.

Ici, on regarde comme on lance l'image docker.
![](./img/docker-terminal.png)

Quand on ouver le navigateur dans `localhost` dans le port `80`, on peut bien voir que WordPress s'ecxécute.

![](./img/wordpress-running-docker.png)