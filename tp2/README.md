# TP2

Dedans le dossier `/src/main/resources/trois_tiers/` regardez la fichier [configuration.xml](./src/main/resources/trois_tiers/configuration.xml). Là-bas la configuration d'initialisation est décrite.

J'ai créé une class 
[Configurator.java](./src/main/java/trois_tiers/Configurator.java) qui contient la logique pour:

- Charger configuration XML
- Obtener les trois `beans` presentation, metier et stockage
- Fermer l'instance de `ApplicationContext`.

Si on compile et on exécute [Configurator.java](./src/main/java/trois_tiers/Configurator.java) on voit sur le terminal.

![](./img/screen.png)

Le script [run.sh](./run.sh) a été mofidié pour compiler et après pour exécuter la class [Configurator.java](./src/main/java/trois_tiers/Configurator.java). Si vous l'exécutez vous aurez le résultat sur votre terminal.
