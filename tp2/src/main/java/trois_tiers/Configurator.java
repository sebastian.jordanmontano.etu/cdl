package trois_tiers;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Configurator {

    public static void main(String[] args) throws Exception {

        System.out.println("------------------------------");
        System.out.println("Charger configuration.xml");
        ClassPathXmlApplicationContext xmlContext = new ClassPathXmlApplicationContext(
                "trois_tiers/configuration.xml");
        System.out.println("------------------------------");

        System.out.println("------------------------------");
        System.out.println("Obtenir Stockage");
        Stockage stockage = xmlContext.getBean("trois_tiers_stockage", Stockage.class);
        System.out.println(stockage);
        System.out.println("------------------------------");

        System.out.println("------------------------------");
        System.out.println("Obtenir Metier");
        Metier metier = xmlContext.getBean("trois_tiers_metier", Metier.class);
        System.out.println(metier);
        System.out.println("------------------------------");

        System.out.println("------------------------------");
        System.out.println("Obtenir Presentation");
        Presentation presentation = xmlContext.getBean("trois_tiers_presentation", Presentation.class);
        System.out.println(presentation);
        System.out.println("------------------------------");

        System.out.println("Fermer configuration.xml");
        xmlContext.close();
    }
}
